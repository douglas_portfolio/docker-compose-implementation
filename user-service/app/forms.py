"""Module for implement forms."""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import InputRequired


class UserForm(FlaskForm):
    """Form for insert user."""

    username = StringField('Username', validators=[InputRequired()])
    password = StringField('Passsword', validators=[InputRequired()])
    birth_date = StringField('Birth Date', validators=[InputRequired()])
    email = StringField('E-mail', validators=[InputRequired()])
    address = StringField('Address', validators=[InputRequired()])
    submit = SubmitField('Submit')
