"""Module to implment models."""

from app import db
import json


class User(db.Model):
    """User class."""

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    birth_date = db.Column(db.String(12))
    email = db.Column(db.String(255), unique=True)
    address = db.Column(db.String(255))

    def __init__(self, username, password, birth_date, email, address):
        """Constructor."""
        self.username = username
        self.password = password
        self.birth_date = birth_date
        self.email = email
        self.address = address

    def __repr__(self):
        """Representation."""
        return '<User %r>' % self.username

    def to_json(self):
        return {
            'username': self.username,
            'password': self.password,
            'birth_date': self.birth_date,
            'email': self.email,
            'address': self.address
        }
