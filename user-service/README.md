# Flask Mapillary
A Flask Application that demonstrates Flask-WTF and Flask-SQLAlchemy using a
SQLite database for Mapillary demonstration.

## Instructions
As always ensure you create a virtual environment for this application and install
the necessary libraries from the `requirements.txt` file.

```
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

Then create the sqlite3 database file and create the tables based on our `app/models.py`.

```
$ touch /tmp/mydatabase.db
$ python
>>> from app import db
>>> db.create_all()
>>> quit()
```

Then start the development server

```
$ python run.py
```

Browse to http://localhost:8080

This api is related with web app serving webpages.

You can also get a list of users using **~/users-json** interface.
