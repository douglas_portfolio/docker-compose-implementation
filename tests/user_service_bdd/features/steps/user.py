from behave import *
import requests
from workflows.user_workflow import Flow as UserFlow
from features.config.environment import Environment
import random
import json

env = Environment()
user_workflow = UserFlow()


@given(u'The server is up')
def givenTheServerIsUp(context):
    r = requests.get('http://server:8080')
    assert r.status_code == 200


@when(u'I create an user')
def whenIcreateAnUser(context):
    json = parse_user(context=context)
    r = user_workflow.create_user(json)
    assert r.status_code == 200


@then(u'Should be possible to get this user.')
def thenShouldBePossibleToGetThisUser(context):
    assert user_workflow.retrieve_user()


def parse_user(context):
    data = {}
    for row in context.table:
        email = row['email'] + str(random.randint(0, 10000))
        data = {
            "username": row['username'],
            "password": row['password'],
            "birth_date": row['birth_date'],
            "email": email,
            "address": row['address']
        }
        break
    print(str(data))
    return data
