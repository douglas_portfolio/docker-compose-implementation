Feature: Create and Recover user
  As a system user
  I want to create an user
  And I want to recover the user from the DB
  So that user was successfully created

  @test
  Scenario: Create an user
    Given The server is up
    When I create an user
      |username|password|birth_date|email|address|
      |myuser|aadkfanad|19/1/1934|user@mail.com|aakdfakjfaldf|

    Then Should be possible to get this user.
