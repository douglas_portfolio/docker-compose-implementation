from features.pages.Page import Page
from selenium.webdriver.common.by import By


class SeeUsersPage(Page):
    TITLE = (By.XPATH, "//*[contains(text(),'Show Users')]")
    TABLE_LINE_XPATH = "//ul/li[contains(text(),'{}')]"

    def __init__(self):
        Page.__init__(self)

    def __is_load__(self):
        self.wait_visibility_of_element_located(SeeUsersPage.TITLE)

    def is_user_visible(self, username):
        table_line = (By.XPATH, "//ul/li[contains(text(),'{}')]".format(username))
        self.wait_visibility_of_element_located(table_line)
        return self