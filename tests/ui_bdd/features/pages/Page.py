from abc import ABCMeta, abstractmethod
from features.driver.factories import DriverFactory
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Page(object, metaclass=ABCMeta):
    _driver = DriverFactory.create_driver('PHANTOMJS')

    @property
    def driver(self):
        return self._driver

    def send_keys(self, locator, text):
        self.driver.find_element(locator[0], locator[1]).send_keys(text)
        return self

    def click(self, locator):
        self.driver.find_element(locator[0], locator[1]).click()
        return self

    def wait_visibility_of_element_located(self, locator):
        WebDriverWait(self.driver, 35).until(EC.visibility_of_element_located(locator))
        return self

    @abstractmethod
    def __is_load__(self):
        pass
