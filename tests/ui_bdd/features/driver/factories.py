from selenium import webdriver
from singleton_decorator import singleton
from abc import ABCMeta, abstractmethod


class DriverFactory(metaclass=ABCMeta):

    @abstractmethod
    def create_driver(browser_name):
        if browser_name.strip().upper() == 'PHANTOMJS':
            driver = Driver('PHANTOMJS', None).instance()
        else:
            driver = Driver('PHANTOMJS', None).instance()

        return driver


@singleton
class Driver:
    DRIVER_PATH = 'features/resources/drivers/{}'
    DRIVER_NAME = {
            'FIREFOX': 'geckodriver',
            'CHROME': 'chromedriver',
            'PHANTOMJS': 'phantomjs'
        }

    def __init__(self, browser_name, browser_options):
        self.__browser_name = browser_name
        self.__browser_options = browser_options
        self.__driver = None

    def instance(self):
        if self.__driver is None:
            self.__driver = webdriver.PhantomJS(executable_path=self.executable_path())
        return self.__driver

    def executable_path(self):
        return self.DRIVER_PATH.format(self.DRIVER_NAME[self.__browser_name.upper()])